import type { Preview } from "@storybook/react";
import { withThemeFromJSXProvider } from "@storybook/addon-styling";
import {getCustomThemeConfig} from '../src/hook/defaultTheme'
import { ThemeProvider } from "styled-components";
// import {GlobalStyles} from '../src/Assets/__fonts/__fonts'

import "@fontsource/inter/400.css"
import "@fontsource/inter/500.css"
import "@fontsource/inter/600.css"
import "@fontsource/inter/700.css"
import "@fontsource/inter/800.css"


export const decorators = [
  withThemeFromJSXProvider({
    themes: {
      light:{...getCustomThemeConfig('KGK','light')},
      dark:{...getCustomThemeConfig('KGK','dark')},
      newTheme:{...getCustomThemeConfig('KGK','dark')},
    },
    defaultTheme: 'light',
    Provider: ThemeProvider,
    // GlobalStyles: GlobalStyles,
  })
]


const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      expanded: true,
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
