import { MouseEventHandler } from "react";
import { KGKCustomThemeSchema } from "../../../hook/defaultTheme";
export interface ButtonProps {
  text?: string;
  primary?: boolean;
  disabled?: boolean;
  size?: "small" | "medium" | "large";
  onClick?: MouseEventHandler<HTMLButtonElement>;
  theme?:typeof KGKCustomThemeSchema
}