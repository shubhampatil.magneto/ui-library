import React from "react";
import styled from "styled-components";
import { ButtonProps } from "./Button.types";
import { StyledButton } from "./Button.style";

const Button: React.FC<ButtonProps> = ({
  size,
  primary,
  disabled,
  text,
  onClick,
  ...props
}:any) => {
  return (
    <StyledButton
      type="button"
      onClick={onClick}
      primary={primary}
      disabled={disabled}
      size={size}
      {...props}>
      {text} 
    </StyledButton>
  );
};

export default Button;
