import React, { useContext } from "react";
import { Button } from "antd";
import styled from "styled-components";
import { StyledAntdButton } from "./AntdButton.style";
import { AntdButtonProps } from "./AntdButton.types";

export function AntdButton(props: AntdButtonProps) {
  const type =
    props.variant === "filled"
      ? "primary"
      : props.variant === "text"
      ? "text"
      : "default";
  return (
    <StyledAntdButton type={type} {...props}>
      {props.children}
    </StyledAntdButton>
  );
}

export default AntdButton;
