import { ButtonProps } from "antd";
import { KGKCustomThemeSchema } from "../../../hook/defaultTheme";
import { ReactNode } from "react";


export interface AntdButtonProps extends ButtonProps {
  children?: ReactNode;
  theme:typeof KGKCustomThemeSchema,
  variant:'filled' | "text" | "outline",
  disabled:boolean
}
