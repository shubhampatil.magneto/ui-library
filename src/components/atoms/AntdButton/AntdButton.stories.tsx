import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import AntdButton from "./AntdButton";

const meta: Meta<typeof AntdButton> = {
  title: "Antd/AntdButton",
  component: AntdButton,
  argTypes: {
    variant: {
      options: ["filled", "outline", "text"],
      control: { type: "radio" },
    },
    disabled: {
      control: { type: "boolean" },
    },
    children: {
      control: { type: "text" },
    },
  },
};

export default meta;
type Story = StoryObj<typeof AntdButton>;
export const Primary: Story = (args) => (
  <AntdButton data-testId="InputField-id" variant="filled" {...args} />
);
Primary.args = {};
