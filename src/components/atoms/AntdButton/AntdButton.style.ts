import { Button } from "antd";
import styled, { css } from "styled-components";
import { AntdButtonProps } from "./AntdButton.types";
import { font } from "../../../styles/mixin";

export const StyledAntdButton = styled(Button)<AntdButtonProps>`
  height: 48px;
  text-transform: capitalize;
  height: 40px;
  padding-left: 16px;
  padding-right: 16px;
  border-radius: 4px;
  ${(props) =>
    props.variant === "filled" &&
    css`
      background-color: ${(props) => {
        console.log(props)
        return props.theme.colors.brand_high
      }};
      ${font({
        color: props.theme.text.dark_high,
        weight: 500,
        size: "14px",
        lineHeight: "20px",
      })}
    `}

  ${(props) =>
    props.variant === "outline" &&
    css`
      border: ${(props) => `1px solid ${props.theme.colors.brand_high}`};
      ${font({
        color: props.theme.text.light_high,
        weight: 500,
        size: "14px",
        lineHeight: "20px",
      })}
      &&&:hover {
        border: ${(props) => `1px solid ${props.theme.colors.brand_high}`};
        border-color: ${(props) => `${props.theme.colors.brand_high}`};
      }
    `}

    ${(props) =>
    props.variant === "text" &&
    css`
      ${font({
        color: props.theme.text.light_high,
        weight: 500,
        size: "14px",
        lineHeight: "20px",
      })}
    `}

    &&&:hover {
    background-color: ${(props) => props.theme.colors.brand_hover};
    color: ${(props) => props.theme.text.dark_high};
  }
  &&&:disabled {
    background-color: ${(props) => props.theme.colors.brand_low};
    color: ${(props) => props.theme.text.light_mid};
  }
`;
