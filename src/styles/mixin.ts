export const font = ({
  color,
  size,
  weight,
  lineHeight,
  space = 0,
}: {
  color: string;
  size: string;
  weight: number;
  lineHeight: string;
  space?: number;
}) => `
  color:${color};
  font-size: ${size};
  font-family:Inter;
  font-weight: ${weight};
  line-height: ${lineHeight};
  letter-spacing: ${space};
`;
