import { theme } from "antd";

export const useMyToken = () => {
  const { token } = theme.useToken();
  return {
    ...token,
    custom: {
      primary: "black",
      seconday:"pink",
    },
  };
};

export * from './defaultTheme'
export * from './theme.hook'