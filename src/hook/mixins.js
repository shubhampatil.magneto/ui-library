import { css } from "styled-components";

export const responsive = {
  mobile: {
    standard: (...args) => css`
      @media (max-width: 991px) {
        ${css(...args)};
      }
    `,
  },
};

export const widthHeight = (w, h = w) => `
  width: ${w}px;
  height: ${h}px;
`;

export const font = {

}