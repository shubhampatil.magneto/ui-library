export type LibType = "KGK";
export type Mode = "light" | "dark";
export interface ThemeArgs {
  libType?: LibType;
  mode?: Mode;
}

export interface CustomTheme {
  KGKlight?: typeof KGKCustomThemeSchema;
  KGKdark?: typeof KGKCustomThemeSchema;
}

export const KGKCustomThemeSchema = {
  colors: {
    primary: "white",
    secondary: "pink",
    brand_high:"#1C1919",
    brand_hover:"#817A70",
    brand_low:"#F9F7F4",
    brand_mid:"#8C8C8C"
  },
  text:{
    light_high:"#383230",
    light_mid:"#8C8C8C",
    low:"#C5C5C5",
    dark_high:"#FFF",
    dark_mid:"#D6D9DF",
    dark_low:"#D6D9DF",
  },
  spacing: {
    padding: {
      small: 10,
      medium: 20,
      large: 30,
    },
    borderRadius: {
      small: 5,
      default: 10,
    },
  },
  typography: {
    type: {
      primary: '"Nunito Sans", "Helvetica Neue", Helvetica, Arial, sans-serif',
      code: '"SFMono-Regular", Consolas, "Liberation Mono", Menlo, Courier, monospace',
    },
    weight: {
      regular: "400",
      bold: "700",
      extrabold: "800",
      black: "900",
    },
    size: {
      s1: 12,
      s2: 14,
      s3: 16,
      m1: 20,
      m2: 24,
      m3: 28,
      l1: 32,
      l2: 40,
      l3: 48,
    },
  },
};

export const KGKDarkCustomThemeSchema = {
  ...KGKCustomThemeSchema,
  colors: {
    primary: "white",
    secondary: "pink",
    brand_high:"#ebe1e1",
    brand_hover:"#817A70",
    brand_low:"#F9F7F4",
    brand_mid:"#8C8C8C"
  },
  text:{
    light_high:"#383230",
    light_mid:"#8C8C8C",
    low:"#C5C5C5",
    dark_high:"#FFF",
    dark_mid:"#D6D9DF",
    dark_low:"#D6D9DF",
  },
};

export const CustomThemeSchemas: CustomTheme = {
  KGKlight: KGKCustomThemeSchema,
  KGKdark: KGKDarkCustomThemeSchema,
};

export const createKGKCustomThemeSchema = ({
  libType = "KGK",
  mode = "light",
}: ThemeArgs) => {
  const selectedThemeSchema = CustomThemeSchemas[`${libType}${mode}`];
  return { ...selectedThemeSchema };
};

export const getCustomThemeConfig = (libType?: LibType, mode?: Mode) => {
  return { ...createKGKCustomThemeSchema({ libType, mode }) };
};
