import React from "react";
import { KGKCustomThemeSchema, getCustomThemeConfig } from "./defaultTheme";
import { createContext } from "react";
import { ThemeProvider } from "styled-components";

export const ThemeContext = createContext<any>(getCustomThemeConfig());

export const CustomThemeProvider: any = ({ libType, mode, children }: any) => {
  const themeOption: any = getCustomThemeConfig(libType, mode);

  return (
    <>
      <ThemeContext.Provider value={themeOption}>
        <ThemeProvider theme={themeOption}>{children}</ThemeProvider>
      </ThemeContext.Provider>
    </>
  );
};

export const UIThemeProvider = React.memo(CustomThemeProvider);

export const useTheme = (): any => React.useContext(ThemeContext);

